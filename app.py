from sanic import Sanic
from sanic.response import json
import revdebug
app = Sanic(__name__)

from skywalking import agent, config
config.init(collector="20.188.46.146:11800", service="Sanic")
agent.start()


def casting_test():
    x = int("3")
    w = float("4.2")
    return x + w






@app.route("/lambda_test")
async def lambda_test(request):
    x = lambda a: a + 10
    revdebug.snapshot("lambda_test")
    return json({"lambda": str(x(casting_test()))}) 

@app.route("/exception")
async def exception_test(request):
    raise Exception("you shall not pass")
    return json({"no": "no"}) 

import random
@app.route("/random")
def random_test(request):
    revdebug.snapshot("random")
    return json({"random": str(random.randrange(1, 10, 1))}) 


def exception_():
    raise Exception("you shall not pass")
 

@app.route("/exeptions")
def exeptions(request):
    for x in range(0,100):
        try:
            exception_()
        except:
            x          
    revdebug.snapshot("exeptions")
    raise Exception("you shall not pass")
    return json({"Hello": "World"}) 


from sanic import Sanic
from sanic.response import json

@app.route("/")
async def test(request):
    json_=json({})
    i=0
    for handler, (rule, router) in app.router.routes_names.items():
        json_.update({})
        i=i+1
    return json_ 




if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)






